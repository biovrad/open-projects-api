<?php
include_once("db_connect.php");
$perPage = 10;
$page = 0;
$url = 'https://freelancehunt.com/';
$project = 'project';

if (isset($_POST['page'])) {
	$page  = $_POST['page']; 
} else { 
	$page=1; 
};

$startFrom = ($page-1) * $perPage;  
$sqlQuery = "SELECT * FROM project ORDER BY id ASC LIMIT $startFrom, $perPage";

$result = mysqli_query($conn, $sqlQuery);

$paginationHtml = '';
while ($row = mysqli_fetch_assoc($result)) {  
	$paginationHtml.='<tr>';  
	$paginationHtml.='<td><a href='.$url.$project.$row["link"].' target="_blank">'.$row["name_project"].'</a></td>';
    $paginationHtml.='<td>'.$row["budget"].'</td>';
    $paginationHtml.='<td>'.$row["user_name"].'</td>';
    $paginationHtml.='<td>'.$row["user_login"].'</td>';
	$paginationHtml.='</tr>';
} 
$jsonData = [
	"html"	=> $paginationHtml,
];
echo json_encode($jsonData); 
