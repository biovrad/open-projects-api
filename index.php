<?php 
include("header.php"); 
include_once("db_connect.php");
require 'Chart/class/ChartJS.php';
require 'Chart/class/ChartJS_Pie.php';

$perPage = 10;
$sqlQuery = "SELECT * FROM project";
$result = mysqli_query($conn, $sqlQuery);
$totalRecords = mysqli_num_rows($result);
$totalPages = ceil($totalRecords/$perPage);

$select = 'SELECT id FROM `project` WHERE budget';
$result500 = mysqli_query($conn, "{$select} >=500")->num_rows;
$result5000 = mysqli_query($conn, "{$select} >=5000")->num_rows;
$result1000 = mysqli_query($conn, "{$select} between 500 AND 1000")->num_rows;
$result5000 = mysqli_query($conn, "{$select} between 1000 AND 5000")->num_rows;

ChartJS::addDefaultColor(['fill' => 'rgba(28,116,190,.8)', 'stroke' => '#1c74be', 'point' => '#1c74be', 'pointStroke' => '#1c74be']);
ChartJS::addDefaultColor(['fill' => 'rgba(212,41,31,.7)', 'stroke' => '#d4291f', 'point' => '#d4291f', 'pointStroke' => '#d4291f']);
ChartJS::addDefaultColor(['fill' => '#FF794B', 'stroke' => '#ff0000', 'point' => '#ff0000', 'pointStroke' => '#ff0000']);
ChartJS::addDefaultColor(['fill' => 'rgba(46,204,113,.8)', 'stroke' => '#2ecc71', 'point' => '#2ecc71', 'pointStroke' => '#2ecc71']);

$array_labels = [
        "Less than 500 UAH",
        "500-1000 UAH",
        "1000-5000 UAH",
        "More than 5000 UAH"
];

$Pie = new ChartJS_Pie('example_pie', 400, 400);
$Pie->addPart($result500);
$Pie->addPart($result1000);
$Pie->addPart($result5000);
$Pie->addPart($result5000);
$Pie->addLabels($array_labels);

?>
<title>Freelancehunt API - open projects</title>
<script src="plugin/simple-bootstrap-paginator.js"></script>
<script src="js/pagination.js"></script>

<?php include('container.php');?>
    <div class="container">
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th><?php echo $Pie ?></th>
                <th>
                    <ul style="margin-bottom: 50% !important;">
                        <li><h4 style="color: rgba(28,116,190,.8)">Less than 500 UAH - <?=$result500?> pcs.</h4></li>
                        <li><h4 style="color: rgba(212,41,31,.7)">500-1000 UAH - <?=$result1000?> pcs.</h4></li>
                        <li><h4 style="color: #FF794B">1000-5000 UAH - <?=$result5000?> pcs.</h4></li>
                        <li><h4 style="color: rgba(46,204,113,.8)">More than 5000 UAH - <?=$result5000?> pcs.</h4></li>
                        <hr>
                    </ul>
                </th>
            </tr>
            </thead>
        </table>
    </div>

<div class="container">
	<div class="row">
		<h2>Freelancehunt API, a list of all open projects</h2>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>Project Name</th>
					<th>Budget (UAN)</th>
					<th>Customer Name</th>
					<th>Customer Login</th>
				</tr>
			</thead>
			<tbody id="content">     
			</tbody>
		</table>   
		<div id="pagination"></div>    
		<input type="hidden" id="totalPages" value="<?php echo $totalPages; ?>">	
	</div>    
</div>
<?php include('footer.php');?>