<link rel="icon" type="image/png" href="#">
</head>
<body class="">
<div role="navigation" class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="https://github.com/freelancehunt/code-test" class="navbar-brand">Test. Open projects.</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="https://apidocs.freelancehunt.com/?version=latest#0eed992e-18f1-4dc4-892d-22b9d896935b">
                        FREELANCEHUNT API 2.0
                    </a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container" style="min-height:500px;">