<?php

$serverName = "localhost";
$username = "root";
$password = "root";
$dbName = "pars_api";
$conn = mysqli_connect($serverName, $username, $password, $dbName) or die("Connection failed: " . mysqli_connect_error());
mysqli_set_charset($conn, 'utf8');

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
