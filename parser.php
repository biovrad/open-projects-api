<?php
function insertDataPdo($name_project, $link, $budget, $user_name, $user_login, \PDO $pdo)
{
    $pdo->query("
              INSERT INTO `project`(`name_project`, `link`, `budget`, `user_name`, `user_login`)
              VALUES
              (
                \"$name_project\",
                \"$link\",
                \"$budget\",
                \"$user_name\",
                \"$user_login\"
              )
        ");
}

function getData($pdo)
{
    $k = 1;
    do {
        $resultArray = getArrayCurl($k);
        foreach ($resultArray['data'] as $item) {
            $amount = isset($item['attributes']['budget']['amount']) ? $item['attributes']['budget']['amount'] : '';
            $currency = isset($item['attributes']['budget']['currency']) ? $item['attributes']['budget']['currency'] : '';
            $project = isset($item['attributes']['name']) ? $item['attributes']['name'] : '';
            $project = str_replace('"', '', $project);
            stripslashes($project);
            $link = isset($item['links']['self']['web']) ? $item['links']['self']['web'] : '';

            $firstName = isset($item['attributes']['employer']['first_name']) ? $item['attributes']['employer']['first_name'] : '';
            $lastName = isset($item['attributes']['employer']['last_name']) ? $item['attributes']['employer']['last_name'] : '';
            $name = $firstName . ' ' . $lastName;

            insertDataPdo(
                $project,
                getLink($link),
                (int)(('RUB' === $currency) ? changeCurrency($amount) : $amount),
                $name,
                isset($item['attributes']['employer']['login']) ? $item['attributes']['employer']['login'] : '',
                $pdo
            );
            echo '-> ';
        }

        if (array_key_exists('last', $resultArray['links'])) {
            $totalPage = getTotalPage($resultArray['links']['last']);
        } else {
            $totalPage = 0;
        }

        print_r('Iteration:' . $k . '. Total:' . $totalPage . '. Count:' . count($resultArray['data']) . "\n");
    } while ($k++ <= $totalPage);

    return (getTotalPage($resultArray['links']['prev']) * 10) + count($resultArray['data']);
}

function getLink($link)
{
    return explode('/project', $link)[1];
}

function getArrayCurl($i = 1)
{
    /** todo реализовать получение ID категорий по нащваниям
     * PHP 1
     * Базы данных 86
     * Веб-программирование 99
     * пока хардкод - [skill_id]=1,86,99
     */

    // todo добавить валидный токен для авторизации
    $token = '';

    if (_is_curl_installed()) {
        $url = "https://api.freelancehunt.com/v2/projects?page[number]={$i}&filter[skill_id]=1,86,99";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        $headers = [];
        $headers[] = "Content-Type: application/json";
        $headers[] = "Accept: application/json";
        $headers[] = "Authorization: Bearer {$token}";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        unset($curl);

        $code = (int)$code;
        $errors = [
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        ];
        try {
            if ($code >= 400)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }

        $resultArray = json_decode($result, true);
        return $resultArray;
    }
}

function getTotalPage($last)
{
    $strVal = str_replace(
        substr(
            $last,
            0,
            (stripos($last, "=") + 1)),
        '',
        $last);

    return substr($strVal, 0, strpos($strVal, "&"));
}

function connectPdo()
{
    $host = '127.0.0.1';
    $db = 'pars_api';
    $user = 'root';
    $pass = 'root';
    $charset = 'utf8mb4';

    $options = [
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

    try {
        $pdo = new \PDO($dsn, $user, $pass, $options);
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }
    return $pdo;
}

function _is_curl_installed()
{
    if (in_array('curl', get_loaded_extensions())) {
        return true;
    } else {
        return false;
    }
}

function getRate()
{
    global $dna;
    $dna = true;
    if (_is_curl_installed()) {
        $url = "https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5";
        $curl = curl_init($url);
        if ($curl) {
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $page = curl_exec($curl);
            curl_close($curl);
            unset($curl);

            $xml = new SimpleXMLElement($page);
            return $xml->row[2]->exchangerate['sale'][0];
        }
    }
}

function changeCurrency($rub)
{
    $rateUAH = (float)getRate();
    if ($rateUAH > 0) {
        return ceil((int)$rub * $rateUAH);
    } else {
        echo $rub;
    }
}

function execute()
{
    $pdo = connectPdo();
    $pdo->query("TRUNCATE TABLE `project`");
    echo "\n<======> Success!!! Records received - " . getData($pdo) . " items! <======> \n";
}

execute();