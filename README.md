Инструкция по запуску парсера
------------
1. перенести проект на локальный компьютер
2. создать базу данных и таблицу
3. запустить парсер в консоли
4. запустить сервер
5. открыть страницу для просмотра


=====**Создать базу данных**=====
>
    CREATE DATABASE IF NOT EXISTS `pars_api` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;


=====**Создать таблицу и индекс**=====

> 
    create table project
    (
      id           int auto_increment
        primary key,
      name_project varchar(128) not null,
      link         varchar(128) not null,
      budget       int          not null,
      user_name    varchar(128) not null,
      user_login   varchar(128) not null
    );
    
>
    create index budget on project (budget);
в файле db_connect.php при необходимости изменить логин и пароль.

=====**Запустить парсер в консоли**=====
>
    ! добавить валидный токен для авторизации, файл parser.php стр.71
    php parser.php
    
=====**Запустить встроенный сервер PHP.**=====
>
    например так:
    php -S 0.0.0.0:8080 -t ~/open-projects-api open-projects-api/index.php
    
=====**Открыть страницу для просмотра**=====
>
    index.php
